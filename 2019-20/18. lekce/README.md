# 18. lekce
- dokončení projektu z minulé lekce
- uživatelské nastavení pomocí formuláře

## Nejjednodušší formulář s jedním polem
HTML:
```html
<input type="text" id="pole" name="pole">
<button onclick="zpracuj()">Zpracuj</button>
```

JS:
```javascript
function zpracuj() {
    hodnota_pole = document.getElementById("pole").value;
    alert(hodnota_pole);
}
```

U typu "radio" je to obdobně, jen je `name` stejné u celé skupiny.

## Kontrola validity JavaScriptem
Řekněme, že chceme mít číslo v intervalu od 0 do 100.
```javascript
function zpracuj() {
    cislo = document.getElementById("cislo").value; // Ziskame hodnotu z pole

    if (isNaN(cislo) || cislo < 0 || cislo > 100) {
        // Provede se, pokud neni zadano cislo NEBO pokud je mimo rozsah
        alert("Nesprávný vstup.");
        return;
    }

    // ...
    // Tento kod se provede pouze v pripade, ze je cislo v poradku
}

```

### Funkce isNaN
- `isNaN` = is not a number
- vrací `true`, pokud nejde o číslo 
- vrací `false`, pokud je vstupem validní číslo (nebo string obsahující pouze číslo)

****

## Další odkazy
- [HTML tag input, použití, atributy](https://www.w3schools.com/tags/tag_input.asp)
- [Typ inputu "radio"](https://www.w3schools.com/tags/att_input_type_radio.asp)

****

## Úkol
Vyberte si vlastní projekt a **začněte na něm pracovat**. Pokud se vám nedaří nic vymyslet, zde je pár tipů:

- Kalkulačka (obecná, geometrická apod.)
- Editor stránek
- Animace
- Prvočísla
- Hra
- Převodník jednotek (fyzikální, měna, metrické/imperiální apod.)
- Nákupní seznam
- Aplikace s TODO listem
- Grafická kostka
- Kvíz nebo test
- Nástroj na tvorbu písma
- Hodiny s datem
- Řazení podle velikosti nebo abecedy

Uvedené nápady mají zcela záměrně různou náročnost a nejsou blíže specifikované, aby si každý mohl vybrat to, co mu půjde nejlépe. Je zcela klíčové, abyste samostnatně tvořili, jinak se to nelze naučit. Pokud nevíte, hledejte na internetu.