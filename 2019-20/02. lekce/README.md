# 2. lekce
- Odkaz, obrázek v HTML (a, img)
    - Klikatelný obrázek jako odkaz
- Atributy tagů
- Ukázka CSS
    - Velikost písma, kurzíva, tučné písmo
    - Font ([Google Fonts](http://fonts.google.com))
    - Úvod do barev ([palety](https://flatuicolors.com))
    - Obtékání elementu
- Zápis CSS do HTML hlavičky (style)


## Další odkazy
- [Jak napsat hvězdičku na české klávesnici](http://znakynaklavesnici.cz/jak-napsat-hvezdicku-na-klavesnici) 
- [Přehled CSS vlastností a hodnot](https://www.jakpsatweb.cz/css/css-vlastnosti-hodnoty-prehled.html)