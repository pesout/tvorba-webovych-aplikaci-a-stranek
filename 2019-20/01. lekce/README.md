# 1. lekce

- Zobrazení zdrojového kódu stránky pomocí CTRL+U
- Instalace editoru (Visual Studio Code)
- Co je a co není HTML (Hypertext Markup Language)
- Tvorba první stránky s použitím výplňkového textu Lorem Ipsum
- HTML tagy pro textový obsah
    - Nadpisy (h1 - h6)
    - Odstavce (p)
    - Kurzíva, tučný text (em, strong)
    - Seznamy (ul, ol, li)
    - Komentáře viditelné pouze v kódu
- Struktura HTML souboru
    - Doctype
    - Hlavička (titulek, kódování češtiny)
    - Tělo stránky
- Doporučená pravidla a konvence pro formátování kódu (odsazování od okraje apod.)
