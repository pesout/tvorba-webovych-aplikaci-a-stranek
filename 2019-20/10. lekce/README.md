# 10. lekce
- Grafický design webu ([prezentace](graficky_design.pdf) Mary Jane Janouškové)
     - Jak a podle čeho vybrat barvy, fonty a layout

****

## Další odkazy
- [Komplementární barvy](https://www.w3schools.com/colors/colors_complementary.asp)
- [Google Fonty](https://fonts.google.com/)

****

## Úkol
Pokračujte v projektech na příští týden s využitím znalostí z této lekce.
