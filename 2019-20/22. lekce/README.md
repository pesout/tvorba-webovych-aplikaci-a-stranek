# 22. lekce (závěrečná)

## Opakování JavaScriptu
- proměnné, přiřazování hodnoty
- jednoduché datové typy a rozdíly mezi nimi
- základní aritmetické operace
- dialogová okna (alert, prompt, confirm)
- podmínky (if, else)
- cykly while a for
- funkce a jejich návratová hodnota
- objektový model (DOM) - modifikace částí stránky pomocí JS ([HTML elementy](https://www.w3schools.com/jsref/prop_html_innerhtml.asp), [CSS styly](https://www.w3schools.com/jsref/prop_html_style.asp) aj.)
- logování do konzole (console.log)
  
## Kam v JavaScriptu dál
- naučit se používat pole (array), bylo probráno pouze v rychlosti
- prozkoumat [události](https://www.jakpsatweb.cz/javascript/udalosti.html) a [jejich využití](https://jecas.cz/pripojeni-udalosti)
- zkusit si něco nakreslit pomocí [canvasu](https://www.w3schools.com/html/html5_canvas.asp)

## Odkazy na tutoriály
- [JS tutorál na JakPsatWeb](https://www.jakpsatweb.cz/javascript/)
- [základy JS na ITnetwork](https://www.itnetwork.cz/javascript/zaklady)
- skvělé stránky vhodné ke studiu jsou také na [JeCas.cz](https://jecas.cz/) 

***

## Dlouhodobý úkol
Je klíčové, abyste pracovali na vlastních projektech, které považujete za přínosné. Když něco nebudete vědět, hledejte na internetu, tak se danou věc nejlépe naučite. Inspiraci můžete čerpat třeba v lekci 18, v některých projektech na [mém GitHubu](https://github.com/pesout) nebo kdekoli po internetu; například [zde](https://skillcrush.com/blog/projects-you-can-do-with-javascript/).
