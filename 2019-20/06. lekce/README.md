# 6. lekce
- Opakování přechozí lekce (absolutní pozicování)

## Úkoly
1. Podívejte se na pátou lekci a popřípadě na odkazy k pochopení problematiky
2. Realizujte následující grafiku v HTML/CSS

![Ukol](ukol.png)
