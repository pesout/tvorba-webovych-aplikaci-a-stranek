# 7. lekce
- Opakování absolutního pozicování
- CSS vlastnost **line-height**
     - Obdoba nastavení řádokování například v MS Word

## Kotvy
- Odkazy dovnitř dokumentu, odrolují stránku na určité místo
- Do atributu **href** u odkazu vložíme **#nazev**, což nás přesune do dokumentu tam, kde je prvek s ID **nazev**

## Relativní pozice
- CSS vlastnost a hodnota **position: relative**
- Definuje změnu pozice oproti místu, kde by byl prvek normálně
- Vizte řešení odkazů uvnitř NAV v přiloženém kódu

## Další odkazy k pochopení problematiky
- [Kotvy v HTML](https://www.jakpsatweb.cz/zalozky.html)
- [Pozicování relativní a absolutní](https://www.jakpsatweb.cz/css/css-pozicovani.html)

## Úkol
- Realizujte následující grafiku v HTML/CSS

![Ukol](ukol.png)

- *Nápověda: jedna z použitých vlastností v mém řešení je border-bottom*
