# 16. lekce
- pokračování na projektu z minulé lekce
    - náhodné umístění jídla `noveJidlo()`
    - logika přidávání a počítání bodů `zkontrolujSnedene()`
    - funkce `konecHry()`
    - dynamické zvyšování obtížnosti (zrychlování)
- opakování

****

## Další odkazy
- viz odkazy z minulé lekce

****

## Úkol
Zjistěte, co je to pole (array) a jak se používá v programování. Ideálně ukažte na nějakém příkladu. 