# 15. lekce
- Pokračování na projektu z minulé lekce
- AND, OR a NOT v podmínkách

## AND (a zároveň), OR (nebo), NOT (negace) v podmínkách
```javascript
if (vyraz_1 && vyraz_2) {
    // kod, ktery se provede, pokud je splnen vyraz_1 A ZAROVEN vyraz_2
} 

if (vyraz_1 || vyraz_2) {
    // kod, ktery se provede, pokud je splnen ALESPON JEDEN (tj. jeden nebo oba) vyrazy
} 

if (! vyraz_1) {
    // kod, ktery se provede, pokud NENI SPLNEN vyraz_1
}
```

****

## Další odkazy
- [Operátory v JavaScriptu](https://www.jakpsatweb.cz/javascript/operatory.html)
- viz odkazy z minulé lekce

****

## Úkol
Dodělejte dvě funkce:

1. `noveJidlo()` 
    - pomocí dvou [náhodných čísel](https://www.jakpsatweb.cz/javascript/objekt-math.html) umístí jídlo na náhodnou pozici
    - zajistí, aby tato náhodná pozice byla v **jiném řádku a sloupci** než je aktuální poloha hráče
    
2. `zkontrolujSnedene()`
    - při zavolání z programu ověří, zda je aktuální pozice jídla stejná jako pozice hráče (tj. jestli je jídlo snědené)

## Bonusový úkol
Udělejte logiku počítání bodů - když je snědené jídlo, přičtěte bod a zavolejte funkci `noveJidlo()`. Počet bodů pak aktualizujte v HTML.