/* Nastaveni ------------- */
const tabulka_max = 20;
const hrac = "&#127877;";
const jidlo = "&#127866;";
const x_start = 7;
const y_start = 7;
const rychlost_start = 100;
/* ----------------------- */

var herni_tabulka = "";
var pos_x = x_start;
var pos_y = y_start;
var rychlost = rychlost_start;
var jdi_nahoru, jdi_dolu, jdi_vlevo, jdi_vpravo;

function polozHrace(x, y) {
    document.getElementById("bunka_" + x + "_" + y).innerHTML = hrac;
}

function polozJidlo(x, y) {
    document.getElementById("bunka_" + x + "_" + y).innerHTML = jidlo;
}

function polozPrazdnou(x, y) {
    document.getElementById("bunka_" + x + "_" + y).innerHTML = "";
}

function konecHry() {
    clearInterval(jdi_nahoru);
    clearInterval(jdi_dolu);
    clearInterval(jdi_vlevo);
    clearInterval(jdi_vpravo);

    pos_x = x_start;
    pos_y = y_start;
    polozHrace(x_start, y_start);
}

function krok(smer) {
    polozPrazdnou(pos_x, pos_y);
    if (smer == "horni") pos_y--; 
    if (smer == "spodni") pos_y++; 
    if (smer == "leva") pos_x--; 
    if (smer == "prava") pos_x++;

    if (pos_x < 0 || pos_x >= tabulka_max || pos_y < 0 || pos_y >= tabulka_max) {
        konecHry();
        return;
    }

    polozHrace(pos_x, pos_y);
}

function pohyb(smer) {
    clearInterval(jdi_nahoru);
    clearInterval(jdi_dolu);
    clearInterval(jdi_vlevo);
    clearInterval(jdi_vpravo);

    if (smer == "horni") {
        jdi_nahoru = setInterval(function(){
            krok(smer);
        }, rychlost);
    }
    if (smer == "spodni") {
        jdi_dolu = setInterval(function(){
            krok(smer);
        }, rychlost);
    }
    if (smer == "leva") {
        jdi_vlevo = setInterval(function(){
            krok(smer);
        }, rychlost);
    }
    if (smer == "prava") {
        jdi_vpravo = setInterval(function(){
            krok(smer);
        }, rychlost);
    }
}

document.addEventListener("keydown", function(klavesa) {
    if (klavesa.which == "37") pohyb("leva");
    if (klavesa.which == "38") pohyb("horni");
    if (klavesa.which == "39") pohyb("prava");
    if (klavesa.which == "40") pohyb("spodni");
});

for (let i = 0; i < tabulka_max; i++) {
    herni_tabulka += "<tr>";
    for (let j = 0; j < tabulka_max; j++) {
        herni_tabulka += "<td id='bunka_" + j + "_" + i + "'>&nbsp;</td>";
    }
    herni_tabulka += "</tr>";
}

document.getElementById("herni_tabulka").innerHTML = herni_tabulka;

polozHrace(x_start, y_start);
