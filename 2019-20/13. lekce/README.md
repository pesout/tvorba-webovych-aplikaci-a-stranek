# 13. lekce
- Opakování, řešení úkolů (podívejte se na odkazy u minulé lekce)
- Objektový model - style

## Modifikace CSS stylů JavaScriptem
```javascript
document.getElementById("vase_zvolene_id").style.cssVlastnost = "nova hodnota";
```

****

## Další odkazy
- [Změna CSS JavaScriptem](https://www.w3schools.com/js/js_htmldom_css.asp)
- [Seznam vlastností](https://www.w3schools.com/jsref/dom_obj_style.asp)

****

## Úkol
Dodělejte denní a noční režim tak, aby se jim přizpůsobovaly všechny elementy. Udělejte hezké CSS styly, aby aplikace vypadala hezky - ideálně i na mobilu.
