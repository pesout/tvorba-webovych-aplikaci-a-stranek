var cas = 0;
var poradi = 1;
var casovac;
var stav = 0;
/*
    0 - vychozi stav
    1 - spustene
    2 - stopnute
*/

function stopky() {
    cas++;
    document.getElementById("cas").innerHTML = cas / 100;
}

function akce() {
    if (stav == 0) { // vychozi stav
        casovac = setInterval(stopky, 10);
        stav = 1;
        document.getElementById("akce").innerHTML = "Stop";
        return;
    }
    if (stav == 1) { // spustene
        clearInterval(casovac);
        stav = 2;
        document.getElementById("akce").innerHTML = "Vymaž";
        return;
    }
    if (stav == 2) { // stopnute
        document.getElementById("cas").innerHTML = 0;
        cas = 0;
        clearInterval(casovac);
        stav = 0;
        document.getElementById("akce").innerHTML = "Start";
        document.getElementById("casy").innerHTML = "";
        return;
    }
}

function lap() {
    document.getElementById("casy").innerHTML += poradi + ". " + cas / 100 + "<br>";
    poradi++;
}

function nocniMod() {
    document.getElementById("telo_stranky").style.backgroundColor = "#222"; 
        // Zmena barvy pozadi stranky
}

function denniMod() {
    document.getElementById("telo_stranky").style.backgroundColor = "#fff";
        // Zmena barvy pozadi stranky
}
