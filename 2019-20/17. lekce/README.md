# 17. lekce
- pokračování na projektu z minulé lekce
    - použití pole pro různá jídla a náhodný výběr
    - tlačítko na začátek hry

****

## Další odkazy
- [Pole v JavaScriptu (1)](https://www.itnetwork.cz/javascript/zaklady/javascript-tutorial-zaciname-s-javascriptem-podminky-cykly-pole-svatky)
- [Pole v JavaScriptu (2)](https://www.jakpsatweb.cz/javascript/pole.html)

****

## Úkol
Zkuste sami přijít na způsob, jakým by se dalo realizovat jednoduché počátační uživatelské nastavení. 

### Tip
Konstanty (`const`) nelze přepisovat, je třeba to realizovat jinak. Nedaly by se poslat vstupní parametry do některé funkce?
