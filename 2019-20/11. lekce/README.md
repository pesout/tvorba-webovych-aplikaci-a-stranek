# 11. lekce
- Úvod do JavaScriptu
     - Přiřazení do proměnné
     - Získání vstupu od uživatele
     - Výpis textu či proměnné
     - Základní matematické operace
     - Větvení (podmínky)
     - Cykly (For, While)
     
Vizte kód a další odkazy.

****

## Další odkazy
- [Jednoduchý JavaScript tutoriál](https://www.jakpsatweb.cz/javascript/) - projděte si prvních 9 odkazů na stránce (až po větvení)
- [Historie a podrobnější popis JS](https://www.itnetwork.cz/javascript/zaklady/javascript-tutorial-uvod-do-javascriptu-nepochopeny-jazyk)

****

## Úkol
Předělejte [výpis slov](vypis-slov.html) tak, aby jich vypsal takové množství, jaké určí uživatel. Stránka se tedy musí zeptat jak na slovo, tak na to, kolikrát se má slovo vypsat.

## Bonusový úkol
1. K čemu jsou příkazy **break** a **continue**?
2. Existuje ještě jiný typ cyklu, než **for** a **while**? Pokud ano, jaký?
