var cas = 0;
var je_spusteno = false; // Promenna typu boolean (pravda/lez)
var casovac;

function stopky() {
    cas++; // Zvetsime promennou cas o 1
    document.getElementById("cas").innerHTML = cas / 100;
        // Vypiseme vysledek do HTML
}

function start() {
    if (je_spusteno) return; // Pokud jiz stopky bezi, nepokracuj
    je_spusteno = true;
    casovac = setInterval(stopky, 10);
        // Funkce se sama spusti kazdych 10 ms
}

function stop() {
  clearInterval(casovac); // Vypni casovac
  je_spusteno = false;
}

function vymaz() {
    // Tuto funkci udelejte kazdy sam :)
}
