# 12. lekce
- Funkce, vstupní a výstupní hodnota
- Použití funkce přímo v JS
- Volání funkcí z HTML 
- Události (events) - onclick, onmouseover
- Časovač - opakované spouštění
- Změna CSS stylu pomocí JS
     
Vizte kód a další odkazy.

****

## Další odkazy
- [Funkce, volání v JS i na základě událostí](https://www.jakpsatweb.cz/javascript/funkce.html)
- [Funkce pro začátečníky](https://www.jakpsatweb.cz/javascript/funkce-prolamy.html)
- [Události](https://www.jakpsatweb.cz/javascript/udalosti.html)
- [Časovač](https://www.jakpsatweb.cz/javascript/casovani.html)
- [Změna HTML pomocí JS](https://www.w3schools.com/jsref/prop_html_innerhtml.asp)
- [Změna stylu pomocí CSS](https://www.w3schools.com/jsref/prop_html_style.asp)
- [Názvy CSS vlastností pro potřeby objektového modelu](https://www.w3schools.com/jsref/dom_obj_style.asp)

****

## Úkol
Dodělejte funkci "vymaž". Cílové chování je takové, že se stopky po kliknutí na tlačítko vynulují a lze je opět spustit kliknutím na tlačítko start.

## Bonusový úkol - varianta 1
Předělejte stopky tak, že budou obsahovat pouze jedno měnící se tlačítko a nikoli tři. 

Výchozí text tlačítka bude "start" a bude spouštět stopky. Po kliknutí se rozběhne časomíra a text tlačítka se změní na "stop". Když uživatel klikne, stopky se zastaví; text tlačítka se změní na "vynulovat" a bude sloužit k návratu do výchozího stavu.

## Bonusový úkol - varianta 2
Naprogramujte funkci LAP. Ta funguje tak, že při kliknutí na příslušné tlačítko se vloží aktuální čas do seznamu pod stopkami, ale časomíra se nezastaví. Můžete k časům přidat i pořadí, ve kterém byly zaznamenány. 

Nepoužívejte document.write, snažte se problém řešit pomocí objektového modelu (DOM) a innerHTML.
