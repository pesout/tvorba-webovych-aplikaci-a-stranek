# 9. lekce
- Flexbox
- CSS vlastnost max-width

## Flexbox

### Zápis
```css
.container {
    display: flex;
}
```

### Probrané vlastnosti a hodnosty
- Vlastnost **flex-wrap** - pravidla pro zalamování, když se nevejde
     - **wrap** - zalamovat;
     - **nowrap** - nezalamovat
- Vlastnost **justify-content** - zarovnání jednotlivých boxů v rámci elementu:
     - **flex-start** - zarovnání k levému okraji;
     - **flex-end** - zarovnání k pravému okraji;
     - **space-between** - "mezera" pouze mezi boxy;
     - **space-around** - "mezera" i na vnějších okrajích

## CSS vlastnost max-width
- V kombinaci se šírkou zadanou v procentech definuje šířku, která je maximální

### Příklad
```css
section {
     width: 95%;
     max-width: 1000px;
}
```
- Šířka elementu je 95 %, ne však více, než 1000 pixelů.

****

## Další odkazy k pochopení problematiky
- [CSS Flexbox Layout Module W3schools](https://www.w3schools.com/csS/css3_flexbox.asp)
- [A Complete Guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [CSS3 Flexbox (česky)](https://www.vzhurudolu.cz/prirucka/css3-flexbox)
- [Příklad na max-width](https://www.jakpsatweb.cz/css/priklady/max-width.html)
- [Mockingbird - aplikace na návrhy rozložení](http://gomockingbird.com)

****

## Úkol
Rozmyslete si za začněte tvořit stránku podle [přiloženého neodborného náčrtu](navrh.pdf). Ten je záměrně vytvořen takto, aby vznikl prostor pro vaši kreativitu. Příští lekce bude pouze konzultace toho, co již máte; pobavíme se o věcech, které vám nefungují. Hotové výtvory budeme procházet až v neděli 1.3.

### Rozvržení
- Stránka musí být responzivní, měla by se hezky zobrazit pro jakékoli rozlišení od 320 pixelů šířky
- Používejte flexbox a @media pravidla
- Absolutní pozicování nezakazuji, ale důrazně doporučuji se mu vyhnout
- Menu bude pouze jako kotvy (odkazy dovnitř dokumentu), protože web je jednostránkový (budete mít jeden HTML soubor). 
- Šířka náčrtu odpovídá 100 % šířky stránky. Výsledek bude tedy zabírat celou šířku okna prohlížeče, přestože je textový obsah uvnitř centrovaný.

### Obsah
- Je na vás, udělejte stránku o tom, co vás zajímá 
