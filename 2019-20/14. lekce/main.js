/* Nastaveni ------------- */
const tabulka_max = 20;
const hrac = "&#127877;";
const jidlo = "&#127866;";
/* ----------------------- */

var herni_tabulka = "";

function polozHrace(x, y) {
    document.getElementById("bunka_" + x + "_" + y).innerHTML = hrac;
}

function polozJidlo(x, y) {
    document.getElementById("bunka_" + x + "_" + y).innerHTML = jidlo;
}

function polozPrazdnou(x, y) {
    document.getElementById("bunka_" + x + "_" + y).innerHTML = "";
}

document.addEventListener("keydown", function(klavesa) {
    if (klavesa.which == "37") alert("leva");
    if (klavesa.which == "38") alert("horni");
    if (klavesa.which == "39") alert("prava");
    if (klavesa.which == "40") alert("spodni");
});

for (let i = 0; i < tabulka_max; i++) {
    herni_tabulka += "<tr>";
    for (let j = 0; j < tabulka_max; j++) {
        herni_tabulka += "<td id='bunka_" + j + "_" + i + "'>&nbsp;</td>";
    }
    herni_tabulka += "</tr>";
}

document.getElementById("herni_tabulka").innerHTML = herni_tabulka;

polozHrace(3,6);
