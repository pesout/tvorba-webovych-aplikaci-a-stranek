# 14. lekce
- Začátek tvorby jednoduché JS hry
- Event Listener
- Tabulka

## Event Listener - spuštění funkce po stisku klávesy
```javascript
document.addEventListener("keydown", function(klavesa) {
    alert("Byla stisknuta klávesa číslo " + klavesa.which);
});
```

## HTML tabulka 2x2
```html
<table>
    <tr>
        <td>Leva horni</td><td>Prava horni</td>
    </tr>
    <tr>
        <td>Leva spodni</td><td>Prava spodni</td>
    </tr>
</table>
```

## CSS vlastnosti k tabulce
```css
table {
    border-spacing: 0; /* Zamezí mezerám mezi okraji buněk */
    border-collapse: collapse; /* Odstraní zdvojené okraje */
}
```

****

## Další odkazy
- [Hra, kterou se inspirujeme](http://sandbox.pesout.eu/hra/)
- [HTML emojis](https://www.w3schools.com/charsets/ref_emoji.asp)
- [Event Listener](https://www.w3schools.com/jsref/met_document_addeventlistener.asp)
- [Keydown event](https://developer.mozilla.org/en-US/docs/Web/API/Document/keydown_event)
- [Tabulky v HTML](https://www.jakpsatweb.cz/html/tabulky.html)
- [Kódy kláves](https://keycode.info/)

****

## Úkol
Tentokrát není. Když vymyslíte vlastní nápad, rád to s vámi proberu po mailu.