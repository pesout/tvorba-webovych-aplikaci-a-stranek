# 4. lekce
- Kontrola a konzultace webových stránek vytvářených samostatně
- Opakovací cvičení (okraje, rámečky, stylování textu, pozadí atd.)
- Další HTML tagy struktury (již známe **header** a **article**)
    - Hlavní obsah stránky (**main**)
    - Část (sekce) stránky (**section**)
    - Postranní obsah (**aside**)
    - Obecný blok (**div**)
    - Označení části textu (**span**)
- Další selektory
    - Identifikátor (**id**) 
        - V CSS `#xyz`
        - Platí pro všechny elementy, které mají `id="xyz"`
        - Podobné, jako třída, ale může ho mít jen jeden element
    - Potomek
        - V CSS `x y`
        - Platí pro všechny elementy Y, které jsou uvnitř X


## Další odkazy ke studiu
- [Všechny CSS selektory](https://jecas.cz/css-selektory)
- [Vysvětlený rozdíl mezi DIV a SPAN](https://www.jakpsatweb.cz/div-span.html)
- [Třídy a identifikátory](https://www.jakpsatweb.cz/css/css-tridy-class.html)
- [Sktrukturní/sémantické elementy v HTML5](https://www.w3schools.com/HTML/html5_new_elements.asp)


## Úkol
1. Zjistěte, jak se v CSS nastavují větší (nebo menší) mezery mezi písmeny
2. Zjistěte, jak se v CSS nastavují větší (nebo menší) mezery mezi slovy
3. Pokuste se vložit stín za písmo
4. Pokuste se vložit stín za nějaký blokový element (např. obrázek)
