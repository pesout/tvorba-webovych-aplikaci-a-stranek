# 8. lekce
- @media pravidla a úvod do [responzivního designu](https://cs.wikipedia.org/wiki/Responzivn%C3%AD_web_design)
- CSS vlastnost display

## Media pravidla

### Zápis:
```css
@media (podminka) {
 /* CSS kód, který platí pouze, pokud podmínka platí */
}
```

### Příklad:
```css
@media (max-width: 600px) {
     /* Tyto deklarace platí jen do 600px šířky */
}

@media (min-width: 601px) and (max-width: 900px) {
     /* Tyto deklarace platí v rozmezí 601px až 900px šířky */
}

@media (min-width: 901px) {
     /* Tyto deklarace platí jen pro více než 901px šířky */
}
```
![Media pravidla](media.png)

## Další odkazy k pochopení problematiky
- [CSS pravidlo @media](https://jecas.cz/media)
- [CSS vlastnost display](https://www.jakpsatweb.cz/css/display.html)
- [Breakpointy v responzivním designu](https://www.vzhurudolu.cz/prirucka/breakpointy)

## Úkol
- Vytvořte responzivní webovou stránku, která se zobrazí tak, jak je uvedeno na obrázcích. Stanovte vhodné breakpointy a **nepoužívejte absolutní pozicování**.
- *Nápověda: použijte CSS vlasnost float*

![Ukol 1](ukol1.png)
![Ukol 2](ukol2.png)
![Ukol 3](ukol3.png)


