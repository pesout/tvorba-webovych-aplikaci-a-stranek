# 5. lekce
- Kontrola úkolu
    - Mezery mezi písmeny (**letter-spacing**)
    - Mezery mezi slovy (**word-spacing**)
    - Stín za textem (**text-shadow**)
    - Stín za blokovým prvkem - např. obrázek (**box-shadow**)
- Další CSS vlastnosti
    - Zaoblení okrajů (**border-radius**)
    - Zvětšení nebo zmenšení všech písmen, kapitálky (**text-transrorm**)

## Absolutní pozicování
- CSS vlastnost a hodnota **position: absolute**
- Umístění prvku na přesné souřadnice pomocí kombinace vlastností 
    - **left** a **top**
    - **right** a **top**
    - **left** a **bottom**
    - **right** a **bottom**
- Počítání souřadnic tak, aby na sebe prvky navazovaly (viz [nákres](nakres.pdf))
    - Např.: *celková šířka = okraje + width*

### Vlastnost z-index
- Vzhledem k tomu, že absolutní pozicování umožňuje umístit cokoli kamkoli, musíme vyřešit překrývání
- Výchozí hodnota je 1
- Prvek překrývá všechny prvky, které mají nižší hodnotu z-index
- Prvek je překrýván všemi prvky, které mají vyšší hodnotu z-index

### Fukce calc()
- Umožňuje počítat s různými jednotkami
- Např.: *width: calc(100% - 50px);*
- Lze využít, když máme třeba padding v pixelech a chceme mít element přes sto procent stránky

### Position: fixed
- Stejné chování jako absolute, rozdíl je v tom, že při scrollování fixovaný prvek zůstává na místě 

*Pozn.: Celé rozvržení pomocí absolutního pozicování se v praxi příliš nevyskytuje, spíše se pracuje s několika takovými prvky, ale je důležité vědět, jak to funguje*

## Další odkazy k pochopení problematiky
- [Box-shadow](https://www.jakpsatweb.cz/css/box-shadow.html)
- [Text-shadow](https://www.jakpsatweb.cz/css/text-shadow.html)
- [CSS pozicování (jakpsaweb.cz)](https://www.jakpsatweb.cz/css/css-pozicovani.html)
- [CSS pozicování (jecas.cz)](https://jecas.cz/position)
- [Z-index](https://www.jakpsatweb.cz/css/z-index.html)
- [Calc() function](https://developer.mozilla.org/en-US/docs/Web/CSS/calc)

## Úkol
- Dobře pochopit problematiku pozicování, na příští lekci si ji ještě zopakujeme
