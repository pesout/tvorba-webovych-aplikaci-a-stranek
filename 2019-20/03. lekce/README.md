# 3. lekce
- CSS externím souborem
- Barva textu, barva pozadí (**color**, **background-color**)
    - Desítkově: **rgb(255,0,255)**
    - Šestnáctkově: **#ff00ff** (**#f0f**)
- Délkové jednoty
    - Typografický bod (**pt**)
    - Obrazový bod (**px**)
    - Procento (**%**)
- Velikosti a okraje
    - Šířka (**width**)
    - Výška (**height**)
    - Vnitřní (**padding**) a vnější (**margin**) okraje
    - Rámeček (**border**)
- Pseudotřída hover
- Vycentrování celé stránky
- Vytvoření jednoduchého menu
- Zobrazení vývojářských nástrojů v prohlížeči (F12)
    - Jak vypadá stránka na různých velikostech displeje


## Další odkazy
- [Přehled CSS vlastností a hodnot](https://www.jakpsatweb.cz/css/css-vlastnosti-hodnoty-prehled.html)
- [Nástroj na výběr barvy](https://www.w3schools.com/colors/colors_picker.asp)
- [Další délkové jednoty v CSS](https://www.jakpsatweb.cz/css/css-jednotky.html)
- [Dropbox - automatické zálohování](https://dropbox.com)
