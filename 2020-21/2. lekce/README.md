# 2. lekce
- Datové typy, funkce, podmínky, operátory

## Funkce
```javascript
const mojeFunkce = (x=1, y=1) => {
     return x * y;
}
```
- nebo zjednodušeně:
```javascript
const mojeFunkce = (x=1, y=1) => x * y;
```
- `x` a `y` jsou vstupní parametry, funkce vrátí (return) jejich součin
- Jakmile funkce něco vrátí, ukončí se
- `mojeFunkce(5, 6)` vrátí tedy 30 - za `x` se dosadí pět, za `y` šest
- `mojeFunkce(5)` vrátí 5 - za `x` se dosadí pět, za `y` výchozí hodnota, tedy jedna

## Typ number (číslo)
- **Vybrané operace**: `+` - sčítání, `-` - odčítání, `*` - násobení, `/` - dělení

## Typ string (textový řetězec)
- **Vybrané operace**:  `+` - skládání
- **Převod stringu na number**: `parseInt("10")`

## Typ boolean
- může nabývat pouze hodnot `true` - pravda, nebo `false` - nepravda

## Operátory
| Operátor | Význam | Příklad, kdy vrátí `true` | Příklad, kdy vrátí `false` |
|:-:|:-:|:-:|:-:|
| `==` | je rovno | `5 == "5"` | `5 == 6` |
| `!=` | není rovno | `5 == 6` | `5 == 5` |
| `===` | je rovno **a zároveň** je stejný i typ | `"5" === "5"` | `5 === "5"` |
| `!==` | není rovno **nebo** není stejný typ (tj. logická negace předchozího) | `5 !== "5"` | `5 !== 5` |
| `>` | je větší | `6 > 5` | `5 > 5` |
| `>=` | je větší nebo rovno | `5 >= 5` | `5 >= 6` |
| `<` | je menší | `5 < 6` | `5 < 5` |
| `<=` | je menší nebo rovno | `5 <= 5` | `6 <= 5` |
| `&&` | a zároveň (logický součin) | `true && true` | `true && false` |
| `׀׀` | nebo (logický součet) | `true` `׀׀` `false` | `false` `׀׀` `false` |
| `!` | negace | `! false` | `! true` |

## Podmínky
```javascript
if (podmínka) {
     alert("Podmínka je splněná");
} else {
     alert("Podmínka není splněná");
}
```
- lze zapsat i bez složených závorek:

```javascript
if (podmínka) alert("Podmínka je splněná");
else alert("Podmínka není splněná");
```

****

## Další odkazy
- Naleznete v loňských lekcích (12 a dál)

****

## Práce do příští konzultace - jednoduchá kalkulačka
1. Vytvořte dvě proměnné, do kterých uložíte dvě čísla získaná na uživatelském vstupu (`prompt`)
2. Vytvořte funkci s názvem `chyba`, která po zavolání vyhodí okénko s chybou a prefixem `Chyba: `
     - například pro `chyba("neznámý problém")` zobrazte [alert](https://cs.wikipedia.org/wiki/Alert) s hláškou `Chyba: neznámý problém`
3. Vytvořte funkci, která vypíše součet, součin, rozdíl a podíl dvou čísel
4. Zavolejte funkci vytvořenou v bodě 3 tak, že vstupními parametry bude uživatelský vstup získaný v bodě 1
5. Ověřte, zda jsou hodnoty, které uživatel zadal, opravdu čísla;  pokud ne, zobrazte chybu pomocí funkce z bodu 2
6. Vytvořte proměnnou, do které uložíte znaménko (`+`, `-`, `*` nebo `/`) z uživatelského vstupu (`prompt`)
7. Upravte funkci vytvořenou v bodě 3 tak, aby kromě čísel na svém vstupu akceptovala jako parametr i znaménko; podle něj vypište právě jeden výsledek (součet, součin, rozdíl, nebo podíl)
8. Ověřte, zda "znaménko" které uživatel zadal, je opravdu jedno znaménko z uvedených čtyř; pokud ne, zobrazte chybu pomocí funkce z bodu 2

## Bonusový úkol
1. Zjistěte, co je to rekurze a jak funguje
2. Vytvořte jednoduchý funkční JS kód, kde prakticky použijete rekurzi
