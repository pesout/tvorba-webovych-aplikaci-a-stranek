# 1. lekce

- Opakování CSS a základů JS

## Proměnné a konstanty v JS
- Pokud ukládáme hodnotu, která se bude měnit, používáme
```javascript
let x = "hodnota";
``` 

- Pokud ukládáme hodnotu, která bude neměnná (konstantní), používáme
```javascript
const x = "hodnota";
``` 

- Deklarace pomocí `var` je validní, ale zastaralá

## Další materiály
- Vizte loňské lekce 1-11
