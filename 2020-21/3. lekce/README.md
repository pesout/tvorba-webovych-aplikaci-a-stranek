# 3. lekce
- Funkce, cykly, switch-case

## Funkce pro ověření, zda jde o číslo
```javascript
const overCislo = (cislo) => {
     if (cislo != false && isNaN(cislo) == false) {
          return true; // Je to cislo
     } else {
          return false; // Neni to cislo
     }
}

const vstup_od_uzivatele = prompt("Zadej cislo", "");

if (overCislo(vstup_od_uzivatele) == true) {
     // Je to cislo
} else {
     // Neni to cislo
}
```

- `isNaN(cislo)` = is not a number = není číslo
     - vrací `true`, pokud zadaná hodnota není číslo
     - vací `false`, pokud zadaná hodnota je číslo
     - `isNaN("aaa")` - vrátí `true`
     - `isNaN("9")` - vrátí `false`


- zápis `cislo != false`
     - vzhledem k tomu, že `"" == false`, takto ověřujeme, že je hodnota definovaná
     - pokud bude `cislo` nevyplněné, výraz `cislo == false` tedy vrátí `true`
     - nás zajímá opak - tedy chceme `true`, právě tehdy, když je číslo vyplněné - `==` změníme na `!=`

- ostranění podmínky fe funkci
     - všimneme si, že výraz `cislo != false && isNaN(cislo) == false` vrací `true` právě tehdy, když celá funkce vrací `true`; také vrací `false` právě tehdy, když celá funkce vrací `false`
     - zjednodušíme tedy zápis tak, že návratová hodnota bude rovnou výsledek výrazu


```javascript
const overCislo = (cislo) => {
     return (cislo != false && isNaN(cislo) == false);
}
```

- pokud funkce má jediný řádek, jako ta naše, lze ji zjednodušit takto:

```javascript
const overCislo = cislo => cislo != false && isNaN(cislo) == false;
```

- nyní můžeme zjednodušit náš výraz pomocí této tabulky ekvivaleních výrazů:

| Dlouhý zápis | Zkrácený zápis |
|:-:|:-:|
| `x == true` | `x` |
| `x == false` | `!x` |
| `x != true` | `!x` |
| `x != false` | `x` |

- `cislo != false` se tedy zjednoduší na `cislo`
- `isNaN(cislo) == false` se tedy zjednoduší na `!isNan(cislo)`
- celá zjednodušená funkce tedy vypadá takto:

```javascript
const overCislo = cislo => cislo && !isNaN(cislo);
```

- podobnou operaci provedeme i s původní podmínkou:

```javascript
if (overCislo(vstup_od_uzivatele)) document.write("Je to cislo");
else document.write("Neni to cislo");
```

## Spojování proměnných a konstant s textem při vypisování

```javascript
const jmeno = prompt("Zadej svoje jmeno");
const vek = prompt("Zadej věk");
const email = prompt("Zadej e-mail");

document.write("Jmenuješ se " + jmeno + ", je ti " + vek + ". Tvůj e-mail je " + email + ".");
```

- tento zápis je nepřehledný; stejnou věc je ale možné realizovat takto:

```javascript
document.write(`Jmenuješ se ${jmeno}, je ti ${vek}. Tvůj e-mail je ${email}.`);
```

- jako první se vyhodnotí výraz v `${}`
- je důležité používat zpětné apostrofy, nikoli uvozovky či cokoli jiného

## Cykly
- Jsou dobře popsané [zde](https://www.itnetwork.cz/javascript/zaklady/javascript-tutorial-cykly-for-while). Zejména je hezký příklad s násobilkou.

## Konstrukce switch-case

- den v týdnu podle čísla:

```javascript
const cislo_dne = prompt("Zadej cislo dne");

let den = "";

switch (cislo_dne) {
  case 1: den = "pondeli"; break;
  case 2: den = "utery"; break;
  case 3: den = "streda"; break;
  case 4: den = "ctvrtek"; break;
  case 5: den = "patek"; break;
  case 6: den = "sobota"; break;
  case 7: den = "nedele"; break;
  default: den = "neznamy den";
}

document.write(`Dnes je ${den}.`)
```

- podle čísla dne se spustí příslušný blok kódu; pokud číslo není nalezeno, spustí se blok za `default:`
- nezapomínejte na příkaz `break` na konci všech bloků krom posledního

****

## Práce do příští konzultace
1. Vytvořte program, který se zeptá uživatele, kolik bude čísel a tento počet uložte do proměnné.
2. Pomocí for cyklu se zeptejte se uživatele na jednotlivá čísla (pokud zadal, že bude 10 čísel, zeptejte se ho 10x).
3. Ověřte, zda jsou zadávané vstupy opravdu čísla. Pokud ne, vypište chybu a ukončete dotazování.
4. Vypište součet uživatelem zadaných čísel.
5. Vypište nejmenší a největší uživatelem zadané číslo.
6. Zkopírujte a přepište příklad tak, abyste použíli while.
7. Vypište počty sudých a lichých čísel. Lichá a sudá čísla jsou definována pouze na oboru přirozených čísel. Záporná tedy igrnorujte.
