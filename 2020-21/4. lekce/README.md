# 3. lekce
- Pole, objekt, DOM

## Pole (array)
- datový typ
- možnost uložit více hodnot do jedné proměnné

- příklad:

```javascript
const pole = [
    "Pondeli", 
    "Utery", 
    "Streda",
    "Ctvrtek",
    "Patek",
    "Sobota",
    "Nedele"
];

const den = 6;
document.write(pole[0]); // vypise "Pondeli"
document.write(pole[3]); // vypise "Streda"
document.write(pole[den - 1]); // vypise "Sobota"
```
- o práci s polem je možné si přečíst [zde](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Indexed_collections#Array_object)

## Objekt
- datový typ, struktura hodnot
- příklad:
```javascript
const objekt = {
     vlastnost: "hodnota",
     cislo: 451
     prvni: "hodnota 1",
     druha: "hodnota 2"   
}

document.write(objekt.cislo); // vypise 451
document.write(objekt.prvni); // vypise "hodnota 1"
document.write(objekt["druha"]); // vypise "hodnota 2"

objekt.jine_cislo = objekt.cislo - 409;
document.write(objekt["jine_cislo"]); // vypise 42
```
- o práci s objekty je možné si přečíst [zde](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_Objects)

## Objektový model (DOM = Document Object Model)
- objektový model je standard, jak pracovat s elementy (styly, obsah, události atd.)
- vizte příklad v přiloženém HTML souboru
- vizte [DOM na ITnetwork](https://www.itnetwork.cz/javascript/zaklady/javascript-tutorial-dom-a-udalosti) a také [DOM na W3schools](https://www.w3schools.com/js/js_htmldom.asp)


## Práce do příští konzultace
1. Najděte si sami na internetu, jak funguje DOM (můžete využít i odkazy výše)
2. Zjistěte, jak upravit elementy, když nemají tag ID a připravte skripty, které to realizují:
     - pomocí názvu tagu
     - pomocí třídy (class)
3. Popište vlastními slovy, kdy se spustí následující události - onclick, onmouseover, onchange, onkeyup, onload
